
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed: ");

        while (!numberScanner.hasNextInt()) {
            System.out.println("That's not an integer. Please try again.");
            System.out.print("Input an integer whose factorial will be computed: ");
            numberScanner.nextLine();
        }

        int num = numberScanner.nextInt();
        long factorial = getFactorial(num);

        if (num < 0) {
            System.out.println("Input number should not be negative!");
        } else {
            System.out.println("The factorial of " + num + " is " + factorial);
        }
    }

    public static long getFactorial(int num) {
        if (num == 0) {
            return 1;
        } else if(num < 0) {
            System.out.println("Invalid input!");
        } else {
            return num * getFactorial(num - 1);
        }
        return 0;
    }
}